k_england = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
	}
}
c_abingdon = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_allerdale = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_anglesey = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_appleby = {
	1444.1.1 = {
		holder = nevillepercy094 # Ralph Neville (Westmorland)
		liege = k_england
	}
}
c_bamburgh = {
	1444.1.1 = {
		holder = nevillepercy078 # Henry Percy
		liege = k_england
	}
}
c_barnstaple = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_bedford = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_berwick = {
	1444.1.1 = {
		holder = nevillepercy078 # Henry Percy
		liege = k_england
	}
}
c_blakeney = {
	1444.1.1 = {
		holder = mowbray_1014 # Duke of Norfolk
		liege = k_england
	}
}
c_bodmin = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_brecknock = {
	1444.1.1 = {
		holder = stafford_1003 # Humphrey Stafford
		liege = k_england
	}
}
c_bridlington = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_bristol = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_buckingham = {
	1444.1.1 = {
		holder = stafford_1003 # Humphrey Stafford
		liege = k_england
	}
}
c_cambridge = {
	1444.1.1 = {
		holder = plantagenet_1054 # Richard of York
		liege = k_england
	}
}
c_canterbury = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_cardigan = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_carlisle = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_carmarthen = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_carnarvon = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_chester = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_clitheroe = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_denbigh = {
	1444.1.1 = {
		holder = plantagenet_1054 # Richard of York
		liege = k_england
	}
}
c_derby = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_doncaster = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_dorchester = {
	1444.1.1 = {
		holder = plantagenet_1001 # Edmund Beaufort
		liege = k_england
	}
}
c_dunster = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_dunwich = {
	1444.1.1 = {
		holder = pole_1018 # William de la Pole
		liege = k_england
	}
}
c_durham = {
	1444.1.1 = {
		holder = nevillepercy105 # Robert Neville
		liege = k_england
	}
}
c_east_sussex = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_essex = {
	1444.1.1 = {
		holder = stafford_1003 # Humphrey Stafford
		liege = k_england
	}
}
c_exeter = {
	1444.1.1 = {
		holder = holland_1005 # John Holland
		liege = k_england
	}
}
c_flint = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_folkestone = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_furness = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_glamorgan = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_gloucester = {
	1444.1.1 = {
		holder = plantagenet_1044 # Humphrey of Gloucester
		liege = k_england
	}
}
c_great_yarmouth = {
	1444.1.1 = {
		holder = mowbray_1014 # Duke of Norfolk
		liege = k_england
	}
}
c_grimsby = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_haughmond = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_hereford = {
	1444.1.1 = {
		holder = stafford_1003 # Humphrey Stafford
		liege = k_england
	}
}
c_hertford = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_hexham = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_holland = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_holme = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_hull = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_huntingdon = {
	1444.1.1 = {
		holder = holland_1005 # John Holland
		liege = k_england
	}
}
c_ipswich = {
	1444.1.1 = {
		holder = pole_1018 # William de la Pole
		liege = k_england
	}
}
c_isle_of_wight = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_kendal = {
	1444.1.1 = {
		holder = nevillepercy094 # Ralph Neville (Westmorland)
		liege = k_england
	}
}
c_kesteven = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_lancaster = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_leicester = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_lincoln = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_liverpool = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_louth = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_lynn = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_macclesfield = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_manchester = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_merioneth = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_middlesex = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_monmouth = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_montgomery = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_nantwich = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_newcastle = {
	1444.1.1 = {
		holder = nevillepercy078 # Henry Percy # King Henry VI of England
		liege = k_england
	}
}
c_northallerton = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_northampton = {
	1444.1.1 = {
		holder = stafford_1003 # Humphrey Stafford
		liege = k_england
	}
}
c_norwich = {
	1444.1.1 = {
		holder = mowbray_1014 # Duke of Norfolk
		liege = k_england
	}
}
c_nottingham = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_oxford = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_pembroke = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_peterborough = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_pickering = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_plymouth = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_pontefract = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_preston = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_radnor = {
	1444.1.1 = {
		holder = plantagenet_1054 # Richard of York
		liege = k_england
	}
}
c_reading = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_ripon = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_rochester = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_rutland = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_scarborough = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_shrewsbury = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_southampton = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_stafford = {
	1444.1.1 = {
		holder = stafford_1003 # Humphrey Stafford
		liege = k_england
	}
}
c_surrey = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_sutton_at_hone = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_taunton = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_truro = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_wareham = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_warwick = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_wells = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_west_suffolk = {
	1444.1.1 = {
		holder = pole_1018 # William de la Pole
		liege = k_england
	}
}
c_west_sussex = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_wiltshire = {
	1444.1.1 = {
		holder = nevillepercy066 # Richard Neville (Salisbury)
		liege = k_england
	}
}
c_winchester = {
	1444.1.1 = {
		holder = plantagenet_1006 # Henry Beaufort
		liege = k_england
	}
}
c_worcester = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_wrexham = {
	1444.1.1 = {
		holder = plantagenet_1043 # King Henry VI of England
		liege = k_england
	}
}
c_york = {
	1444.1.1 = {
		holder = plantagenet_1054 # Richard of York
		liege = k_england
	}
}
d_york = {
	1444.1.1 = {
		holder = plantagenet_1054 # Richard of York
		liege = k_england
	}
}