absolutism_lifestyle = {
	
	is_highlighted = {
		has_cultural_era_or_later = culture_era_early_modern
	}
	
	is_valid = {
#		has_cultural_era_or_later = culture_era_early_modern
		is_independent_ruler = yes
	}
	
	xp_per_level = 4500
	base_xp_gain = 0
}