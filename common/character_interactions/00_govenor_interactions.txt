﻿



grant_governorship_interaction = {
	category = interaction_category_vassal
	use_diplomatic_range = yes
	ignores_pending_interaction_block = yes
	target_type = title
	target_filter = actor_domain_titles
	interface_priority = 80	
	#force_notification = yes
	greeting = positive
	#notification_text = GRANT_INDEPENDENCE_INTERACTION_NOTIFICATION

	desc = "Grants governorship!"
	
	is_shown = {
		scope:actor = {
			OR = {
				is_independent_ruler = yes
				highest_held_title_tier > county
			}
			OR = {
				var:office_slots = { compare_value < scope:actor.governor_value }
				NOT = { has_variable = office_slots }
			}
			
		}
		NOT = { scope:recipient = scope:actor }
	}

	is_valid_showing_failures_only = {
		NOT = { scope:actor = { is_at_war_with = scope:recipient } }
		scope:actor = {
			OR = {
				is_independent_ruler = yes
				highest_held_title_tier > county
			}
			#var:office_slots = { compare_value < scope:actor.governor_value }
		}
	}
	can_be_picked_title = {
		scope:target = {
			holder = scope:actor
			NOT = { has_variable_list = is_governed }
		}
		NOT = {
			scope:target = scope:actor.primary_title
		}
	}
	auto_accept = yes

	on_accept = {
		scope:actor = {
			if = {
				limit = {
					NOT = { has_variable = office_slots }
				}
				set_variable = {
					name = office_slots
					value = 0
				}
				
			}

			change_variable = { name = office_slots add = 1 }
			
			set_variable = {
				name = office_slots_max
				value = governor_value 
			}
			
			
		}
		scope:target = {
			add_to_variable_list = {
				name = is_governed
				target = scope:recipient
			}
			add_county_modifier = province_govenor
		}
		scope:recipient = {
			#Send notifications
		
			add_to_variable_list= {
				name = governorship
				target = scope:actor
			}
	
			add_to_variable_list = {
				name = govern_titles
				target = scope:target
			}
		
			add_opinion = {
				modifier = govenor_appointment_opinion
				target = scope:actor
				opinion = 15
			}

		}
		scope:actor = {
			clear_variable_list = governors
			
			every_vassal_or_below = {
				limit = {
					has_variable_list = governorship
				}
				scope:actor = {
					add_to_variable_list = {
						name = governors
						target = prev
					}
				}				
			}			
			every_courtier = {
				limit = {
					has_variable_list = governorship
				}
				scope:actor = {
					add_to_variable_list = {
						name = governors
						target = prev
					}
				}				
			}
			every_close_or_extended_family_member = {
				limit = {
					has_variable_list = governorship
				}
				scope:actor = {
					add_to_variable_list = {
						name = governors
						target = prev
					}
				}				
			}
			set_variable = { name = needs_refresh value = 1 }
		}
		
	}

	ai_will_do = {
		base = 0 #The AI should never do this!

	}
}

remove_governorship_interaction = {
	category = interaction_category_vassal
	use_diplomatic_range = no
	ignores_pending_interaction_block = yes
	target_type = title
	target_filter = actor_domain_titles
	interface_priority = 80	
	#force_notification = yes
	greeting = negative
	#notification_text = GRANT_INDEPENDENCE_INTERACTION_NOTIFICATION

	desc = "Removes governorship!"
	
	is_shown = {
		scope:actor = {
			OR = {
				is_independent_ruler = yes
				highest_held_title_tier > county
			}
		}
		
		NOT = { scope:recipient = scope:actor }

		scope:recipient = {
			has_variable_list = governorship
		}
	}

	is_valid_showing_failures_only = {
		scope:actor = {
			OR = {
				is_independent_ruler = yes
				highest_held_title_tier > county
			}
			
		}
	}
	can_be_picked_title = {
		scope:target = {
			holder = scope:actor
			has_variable_list = is_governed
			scope:recipient = {
				is_target_in_variable_list = { name = govern_titles target = scope:target }
			}
		}
	}

	auto_accept = yes

	on_accept = {
		scope:actor = {
		
			change_variable = { name = office_slots subtract = 1 }
			
			set_variable = {
				name = office_slots_max
				value = governor_value 
			}			
		}
		scope:target = {
			remove_county_modifier = province_govenor
			clear_variable_list = is_governed
		}

		scope:recipient = {
			#Send notifications
		
			remove_list_variable = { name = govern_titles target = scope:target }
			if = {
				limit= {
					has_variable_list = govern_titles
				}
			}
			else = {
				clear_variable_list = governorship
			}
			
			#remove this make negative
			#fired_from_council_position_effect = yes
 
			add_opinion = {
				modifier = fired_govenor_appointment_opinion
				target = scope:actor
				opinion = -15
			}
		}
		scope:actor = {
			clear_variable_list = governors
			
			every_vassal_or_below = {
				limit = {
					has_variable_list = governorship
				}
				scope:actor = {
					add_to_variable_list = {
						name = governors
						target = prev
					}
				}				
			}			
			every_courtier = {
				limit = {
					has_variable_list = governorship
				}
				scope:actor = {
					add_to_variable_list = {
						name = governors
						target = prev
					}
				}				
			}
			every_close_or_extended_family_member = {
				limit = {
					has_variable_list = governorship
				}
				scope:actor = {
					add_to_variable_list = {
						name = governors
						target = prev
					}
				}				
			}
			set_variable = { name = needs_refresh value = 1 }
		}
	}

	ai_will_do = {
		base = 0 #The AI should never do this!

	}
}