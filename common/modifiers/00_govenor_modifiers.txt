﻿
province_govenor = {
	icon = economy_positive
	monthly_county_control_change_add = 0.1
	county_opinion_add = 20
	levy_size = 0.3
	tax_mult = 0.5
	development_growth_factor = small_development_growth_gain
}
